import http from 'http';

const requestListener = (req, res) => {
    const data = JSON.stringify({
        date: new Date().toISOString(),
        geo: 'geo:51.2993,9.491;u=463000',
    });
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(data);
};

const server = http.createServer(requestListener);
server.listen(process.env.PORT || 8080, (event) => {
    console.log('HTTP server is listening.');
});
