function getElementByName(root, name) {
  if (!root) throw new Error('Cannot inspect undefined!');
  return root.elements.find((ast) => ast.type === 'element' && ast.name === name);
}

function getElementsByName(root, name) {
  if (!root) throw new Error('Cannot inspect undefined!');
  return root.elements.filter((ast) => ast.type === 'element' && ast.name === name);
}

function requireElementByName(root, name) {
  const found = getElementByName(root, name);
  if (!found) throw new Error(`Could not find ${name}.`);
  return found;
}

function getText(root) {
  const textElement = root.elements.find((ast) => ast.type === 'text');
  if (textElement) return textElement.text;
}

function setText(root, text) {
  const textElement = root.elements.find((ast) => ast.type === 'text');
  if (!textElement) throw new Error('Element has no text child.');
  textElement.text = text;
}

export function getPositionAtTime(root, date) {
  const gpx = requireElementByName(root, 'gpx');
  const track = requireElementByName(gpx, 'trk');
  const segments = getElementsByName(track, 'trkseg');

  let lastDate;
  let lastPosition;
  segments.forEach((segment) => {
    const points = segment.elements.filter((element) => {
      return element.type === 'element' && element.name === 'trkpt';
    });
    points.forEach((point) => {
      if (!point.attributes) {
        console.dir(point);
        console.warn(`${path} has a point without attributes!`);
        return;
      }
      if (!point.attributes.lat) {
        console.warn(`${path} has a point without lat value!`);
        return;
      }
      const extensions = requireElementByName(point, 'extensions');
      const gpxtpx = requireElementByName(extensions, 'gpxtpx:TrackPointExtension');
      let currentPoint = {
        lat: point.attributes.lat,
        lon: point.attributes.lon,
        accuracy: getText(requireElementByName(gpxtpx, 'accuracy')),
        time: new Date(getText(requireElementByName(point, 'time'))),
      };
      if ((!lastDate || currentPoint.time > lastDate) && currentPoint.time < date) {
        lastDate = currentPoint.time;
        lastPosition = currentPoint;
      }
    });
  });
  if (!lastPosition) return;
  return {
    date: new Date(lastDate),
    geo: `geo:${lastPosition.lat},${lastPosition.lon};u=${lastPosition.accuracy}`,
  };
}

/**
 * @returns {Date}
 */
export function getStartDate(root) {
  const gpx = requireElementByName(root, 'gpx');
  const track = requireElementByName(gpx, 'trk');
  const segment = getElementByName(track, 'trkseg');
  const point = getElementByName(segment, 'trkpt');
  return new Date(getText(requireElementByName(point, 'time')));
}
