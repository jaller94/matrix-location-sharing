import fsPromises from 'fs/promises';
import http from 'http';
import convert from 'xml-js';
import {
    getPositionAtTime,
    getStartDate,
} from './gpx.mjs';

const startDate = new Date();
const xml = await fsPromises.readFile('route.gpx', 'utf8');
const doc = convert.xml2js(xml);
const startDateOfGpx = getStartDate(doc);

const requestListener = (req, res) => {
    const now = Date.now();
    const msFromReplayStartToNow = now - startDate.getTime();
    const fakeTime = new Date(startDateOfGpx.getTime() + msFromReplayStartToNow);
    const point = getPositionAtTime(doc, fakeTime);
    if (!point) return;
    const msFromGpxStartToPoint = new Date(point.date).getTime() - startDateOfGpx.getTime();
    // If the GPX track had started with the replay, what time is the point at?
    const pointDateBroughtToThePresent = new Date(startDate.getTime() + msFromGpxStartToPoint);
    const data = JSON.stringify({
        date: pointDateBroughtToThePresent.toISOString(),
        geo: point.geo,
    });
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(data);
};

const server = http.createServer(requestListener);
server.listen(process.env.PORT || 8080, (event) => {
    console.log('HTTP server is listening.');
});

console.log(`Start of the GPX track: ${startDateOfGpx}`);
