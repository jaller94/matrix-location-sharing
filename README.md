# Location sharing experiments

This is an experimental implementation of location sharing in Matrix.  

## Sources

The bot can fetch its location information from a configurable source, e.g. an HTTP endpoint or a GPS module.

Currently, only an HTTP endpoint is supported as a source.

### Output modes

Currently, only the `stream` mode is supported.

* `locations` – Posts `m.location` messages as defined in ([MSC3488](https://github.com/matrix-org/matrix-spec-proposals/pull/3488)).
* `stream` – Defines a beacon and streams persistent location events as defined in MSC3489 and MSC3672.
* `ephemeral-stream` – Defines a beacon and streams ephemeral location events as defined in MSC3672.

## Relevant MSCs

* [MSC3488: Extending events with location data](https://github.com/matrix-org/matrix-spec-proposals/pull/3488)
* [MSC3489: Sharing streams of location data with history](https://github.com/matrix-org/matrix-spec-proposals/pull/3489)
* [MSC3672: Sharing ephemeral streams of location data](https://github.com/matrix-org/matrix-spec-proposals/pull/3672)

## Setup
You need Bun 1.1 to run this project.

1. Rename `config/shares.sample.json` to `shares.json` and configure your bot.
2. Rename `run.sh.sample` to `run.sh` and enter the credentials of your Matrix bot account.
4. Open a terminal and run `node src-sources/http-gpx/index.mjs`.
5. Open another terminal and run `./run.sh`.

## License

(C) 2022 [New Vector Ltd.](https://element.io/about)
(C) 2024 [Christian Paul](https://chrpaul.de/about)

Apache License 2.0
