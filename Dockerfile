FROM node:21 AS BUILD-BACKEND

# Install app dependencies
WORKDIR "/app"
COPY . /app/
RUN npm ci

FROM oven/bun:1

ENV NODE_ENV production
ENV PATH_CONFIG /config
ENV PATH_DATA /data

WORKDIR "/app"

COPY . /app/
COPY --from=BUILD-BACKEND /app/node_modules/ node_modules/

# Make the build fail if @matrix-org/matrix-sdk-crypto-nodejs wasn't installed correctly.
# error: Cannot find module "@matrix-org/matrix-sdk-crypto-nodejs" from "/home/bun/.bun/install/cache/matrix-bot-sdk@0.7.1/lib/e2ee/CryptoClient.js"
RUN bun test-sdk.ts

# HTTP port
EXPOSE 8080
CMD ["bun", "run", "src/index.ts"]
