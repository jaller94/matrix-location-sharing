import * as fsPromises from 'node:fs/promises';
import { join } from 'node:path';
import {
    ICryptoStorageProvider,
    LogLevel,
    LogService,
    MatrixClient,
    SimpleFsStorageProvider,
    RustSdkCryptoStorageProvider,
} from 'matrix-bot-sdk';

const PATH_CONFIG = process.env.PATH_CONFIG || './config';
const PATH_DATA = process.env.PATH_DATA || './data';

const fetchPositionData = async (): Promise<{geo: string, date: string}> => {
    if (process.env.INVOXIA_AUTH && process.env.INVOXIA_URL) {
        const res = await fetch(process.env.INVOXIA_URL, {
            headers: {
                Authorization: `Basic ${btoa(process.env.INVOXIA_AUTH)}`,
            },
        });
        if (!res.ok) {
            throw Error(`Invoxia API: HTTP ${res.status} ${res.statusText}`);
        }
        const data = await res.json();
        if (!Array.isArray(data)) {
            console.warn('data', data);
            throw Error('Expected Invoxia response to be an array');
        }
        const mostRecentEvent = data[0];
        if (!mostRecentEvent) {
            throw Error('Expected Invoxia response to contain at least one location');
        }
        return {
            geo: `geo:${mostRecentEvent.lat},${mostRecentEvent.lng};u=${mostRecentEvent.precision}`,
            date: new Date().toISOString(),// mostRecentEvent.datetime,
        }
    }
    const res = await fetch('http://localhost:8080');
    const lastPosition = await res.json() as any;
    return lastPosition;
}

LogService.setLevel(LogLevel.ERROR);

// We'll want to make sure the bot doesn't have to do an initial sync every
// time it restarts, so we need to prepare a storage provider. Here we use
// a simple JSON database.
await fsPromises.mkdir(PATH_DATA, {recursive : true});
const storage = new SimpleFsStorageProvider(join(PATH_DATA, 'storage.json'));
let crypto: ICryptoStorageProvider | undefined;

if (process.env.MATRIX_ENCRYPTION) {
    await fsPromises.mkdir(join(PATH_DATA, 'crypto'), {recursive : true});
    crypto = new RustSdkCryptoStorageProvider(join(PATH_DATA, 'crypto'));
}

async function loadAccessToken() {
    return await fsPromises.readFile(join(PATH_DATA, 'accesstoken.txt'), 'utf8');
}

async function loginAndSaveToken(username: string, password: string): Promise<string> {
    const { MatrixAuth } = require('matrix-bot-sdk');
    const auth = new MatrixAuth(MATRIX_SERVER);
    const result = await auth.passwordLogin(username, password);
    await fsPromises.writeFile(join(PATH_DATA, 'accesstoken.txt'), result.accessToken, 'utf8');
    return result.accessToken;
}

type Share = {
    type: LocationSharingType.Locations;
    interval: NodeJS.Timeout;
} | {
    type: LocationSharingType.EphemeralStream | LocationSharingType.Stream;
    interval: NodeJS.Timeout;
    beaconEventId: string;
};

const liveShares: Map<string, Share> = new Map();

enum LocationSharingType {
    Locations,
    Stream,
    EphemeralStream,
};

async function startBot(accessToken: string): Promise<void> {
    //await login(localStorage, baseUrl, myUserId, myPassword);
    //console.log(localStorage.getItem('accessToken'));
    const client = new MatrixClient(
        MATRIX_SERVER,
        accessToken,
        storage,
        crypto
    );

    const startShare = async(roomId: string) => {
        const liveShare = liveShares.get(roomId);
        let type = LocationSharingType.Stream as LocationSharingType;
        let beaconEventId = '';
        if (liveShare) {
            await client.sendNotice(roomId, 'Location sharing is already on.');
            return;
        }
        if ([LocationSharingType.EphemeralStream, LocationSharingType.Stream].includes(type)) {
            beaconEventId = await updateBeaconState(client, roomId, true, config.beaconInfo);
        }
        const sendLocationUpdate = async() => {
            try {
                switch (type) {
                    case LocationSharingType.EphemeralStream:
                        await postEphemeralBeaconInfo(client, roomId, beaconEventId!);
                        break;
                    case LocationSharingType.Locations:
                        await postLocation(client, roomId);
                        break;
                    case LocationSharingType.Stream:
                        await postBeaconData(client, roomId, beaconEventId!);
                        break;
                }
            } catch (err) {
                console.warn(err);
                console.warn('Error in sendLocationUpdate');
            }
        };
        sendLocationUpdate();
        const interval = setInterval(sendLocationUpdate, config.updateInterval * 1000);

        if (type === LocationSharingType.Locations) {
            liveShares.set(roomId, {
                interval,
                type,
            });
        } else {
            liveShares.set(roomId, {
                beaconEventId: beaconEventId,
                interval,
                type,
            });
        }

        await client.sendNotice(roomId, 'Location sharing started.');
    }

    const stopShare = async(roomId: string) => {
        const liveShare = liveShares.get(roomId);
        if (!liveShare) {
            await client.sendNotice(roomId, 'Location sharing is not on.');
            return;
        }
        clearInterval(liveShare.interval);
        liveShares.delete(roomId);
        if ([LocationSharingType.EphemeralStream, LocationSharingType.Stream].includes(liveShare.type)) {
            await updateBeaconState(client, roomId, false, config.beaconInfo);
        }
        
        await client.sendNotice(roomId, 'Location sharing stopped.');
    };

    client.on('room.message', async (roomId, event) => {
        // Don't handle events that don't have contents (they were probably redacted)
        if (!event['content']) return;

        // Don't handle non-text events
        if (event['content']['msgtype'] !== 'm.text') return;

        // We never send `m.text` messages so this isn't required, however this is
        // how you would filter out events sent by the bot itself.
        if (event['sender'] === await client.getUserId()) return;

        if (event.content.body === 'start') {
            await startShare(roomId);
        } else if (event.content.body === 'stop') {
            await stopShare(roomId);
        }
    });

    const config = await loadConfig(client);

    async function shutdown() {
        console.info('Shutdown signal received.');
        client.stop();
        for (const [roomId, liveShare] of liveShares.entries()) {
            clearInterval(liveShare.interval);
            await updateBeaconState(client, roomId, false, config.beaconInfo);
        }
    }

    
    process.on('SIGINT', shutdown);
    // process.on('SIGTERM', shutdown);
    
    await client.start();
    console.log('Bot is listening.');

    for(const room of config.rooms) {
        await startShare(room);
    }
}

async function loadConfig(client: MatrixClient) {
    let config: Record<string, unknown>;
    if (process.env.MATRIX_CONFIG_ROOM) {
        config = await client.getRoomStateEvent(process.env.MATRIX_CONFIG_ROOM, 'de.chrpaul.userconfig', await client.getUserId());
    } else {
        config = JSON.parse(await fsPromises.readFile(join(PATH_CONFIG, 'shares.json'), 'utf8'));
    }
    return {
        beaconInfo: {},
        rooms: [],
        updateInterval: 300,
        ...config
    };
}

/**
 * @returns resolves to the event ID that represents the event
 */
async function updateBeaconState(
    client: MatrixClient,
    roomId: string,
    enabled = true,
    beaconInfo = {},
): Promise<string> {
    const matrixUserId = await client.getUserId();
    // const content = {
    //     'org.matrix.msc3489.beacon_info': {
    //         'description': 'Test Tracker', // same as an `m.location` description
    //         'timeout': 1 * 60 * 60 * 1000, // how long from the last event until we consider the beacon inactive in milliseconds
    //         'live': enabled,
    //     },
    //     'org.matrix.msc3488.ts': startDate, // creation timestamp of the beacon on the client
    //     'org.matrix.msc3488.asset': {
    //         'type': 'm.self' // the type of asset being tracked as per MSC3488
    //     }
    // };
    // return client.sendStateEvent(roomId, `org.matrix.msc3489.beacon_info.${matrixUserId}`, matrixUserId, content);
    const content = {
        ...beaconInfo,
        'live': enabled,
        'timeout': 1 * 60 * 60 * 1000, // how long from the last event until we consider the beacon inactive in milliseconds
        'org.matrix.msc3488.ts': Date.now(), // creation timestamp of the beacon on the client
        'org.matrix.msc3488.asset': {
            'type': 'm.self' // the type of asset being tracked as per MSC3488
        }
    };
    return client.sendStateEvent(roomId, 'org.matrix.msc3672.beacon_info', matrixUserId, content);
}

/**
 * Posts an event in the room to update the beacon's data.
 * @returns resolves to the event ID that represents the event
 */
async function postBeaconData(
    client: MatrixClient,
    roomId: string,
    beaconEventId: string
): Promise<string> {
    const lastPosition = await fetchPositionData();
    const content = {
        'm.relates_to': { // from MSC2674: https://github.com/matrix-org/matrix-doc/pull/2674
            'rel_type': 'm.reference', // from MSC3267: https://github.com/matrix-org/matrix-doc/pull/3267
            'event_id': beaconEventId,
        },
        'org.matrix.msc3488.location': {
            uri: lastPosition.geo,
            // description: 'Arbitrary beacon information'
        },
        'org.matrix.msc3488.ts': Date.parse(lastPosition.date),
    };
    // return client.sendEvent(roomId, 'org.matrix.msc3489.beacon', content);
    return client.sendEvent(roomId, 'org.matrix.msc3672.beacon', content);
}

async function postEphemeralBeaconInfo(client: MatrixClient, roomId: string, beaconEventId: string) {
    const lastPosition = await fetchPositionData();
    const content = {
        'm.relates_to': { // from MSC2674: https://github.com/matrix-org/matrix-doc/pull/2674
            'rel_type': 'm.reference', // from MSC3267: https://github.com/matrix-org/matrix-doc/pull/3267
            'event_id': beaconEventId,
        },
        'org.matrix.msc3488.location': {
            uri: lastPosition.geo,
            // description: 'Arbitrary beacon information'
        },
        'org.matrix.msc3488.ts': Date.parse(lastPosition.date),
    };
    // return client.sendEvent(roomId, 'org.matrix.msc3489.beacon', content);
    return sendEphemeralEvent(client, roomId, 'org.matrix.msc3672.beacon', content);
}

async function sendEphemeralEvent(client: MatrixClient, roomId: string, type: string, content: Record<string, unknown>) {
    const txnId = `${Math.random()}`;
    return client.doRequest(
        'PUT',
        `/_matrix/client/v1/rooms/${encodeURIComponent(roomId)}/ephemeral/${encodeURIComponent(type)}/${txnId}`,
        null,
        content
    );
}

async function postLocation(client: MatrixClient, roomId: string) {
    const lastPosition = await fetchPositionData();
    const content = {
        msgtype: 'm.location',
        body: `User Location ${lastPosition.geo} at ${new Date(lastPosition.date).toISOString()}`,
        geo_uri: lastPosition.geo,
        'org.matrix.msc3488.location': {
            uri: lastPosition.geo,
        },
        'org.matrix.msc3488.asset': {
            type: 'm.self',
        },
        'org.matrix.msc1767.text': `User Location ${lastPosition.geo} at ${new Date(lastPosition.date).toISOString()}`,
        'org.matrix.msc3488.ts': Date.parse(lastPosition.date),
    };
    return client.sendMessage(roomId, content);
}

function requireEnv(name: string): string {
    const value = process.env[name];
    if (!value) {
        throw Error(`Required ENV variable undefined: ${name}`);
    }
    return value;
}

async function main() {
    let accessToken = process.env['MATRIX_ACCESS_TOKEN'];
    
    
    if (!accessToken) {
        const MATRIX_USER = requireEnv('MATRIX_USER');
        const MATRIX_PASSWORD = requireEnv('MATRIX_PASSWORD');
    
        try {
            accessToken = await loadAccessToken();
        } catch (error: any) {
            if (error.code !== 'ENOENT') {
                throw error;
            }
        }
        if (!accessToken) {
            accessToken = await loginAndSaveToken(MATRIX_USER, MATRIX_PASSWORD);
        }
    }

    console.debug('Starting bot…');
    await startBot(accessToken);
}

const MATRIX_SERVER = requireEnv('MATRIX_SERVER');

console.debug('Starting main…');
main().catch(console.error);
